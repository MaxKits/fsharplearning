﻿module AzureStorageCleaner
open FSharp.Azure.StorageTypeProvider

type Azure = AzureTypeProvider<"UseDevelopmentStorage=true">


let findFiles (fromTime:System.DateTime) (toTime:System.DateTime) (containersRegex:string) =
    let isTargetContainerPredicate (blob:Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer) =
        let blobNameRegex = System.Text.RegularExpressions.Regex.Match(blob.Name,containersRegex);
        blobNameRegex.Success
        
    let isTargetBlobGzLogPredicate (blob:Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob) =
        let blobNameRegex = System.Text.RegularExpressions.Regex.Match(blob.Name,@"(\d{4})(\d{2})(\d{2})-(\d{2}).log.gz");
        if (blobNameRegex.Success) then
            let dt = System.DateTime.ParseExact (blobNameRegex.Groups.Item(1).Value+blobNameRegex.Groups.Item(2).Value+blobNameRegex.Groups.Item(3).Value ,"yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture ,System.Globalization.DateTimeStyles.None)
            dt < toTime && dt > fromTime;
        else 
            false
            
    let isTargetBlobAnyFilePredicate (blob:Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob) =
            blob.Properties.LastModified.HasValue && blob.Properties.LastModified.Value.DateTime< toTime && blob.Properties.LastModified.Value.DateTime > fromTime;
    
    // we don't need it since we force azure to return flaten list of blobs
    let rec getBlocks (iListBlobItems:seq<Microsoft.WindowsAzure.Storage.Blob.IListBlobItem>) = 
        iListBlobItems
            |> Seq.map (fun v ->  
                    match v with 
                    | :? Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob as b when isTargetBlobGzLogPredicate b -> Seq.singleton b ;
                    | :? Microsoft.WindowsAzure.Storage.Blob.CloudBlobDirectory as d -> getBlocks (d.ListBlobs ()) |> Seq.concat ;
                    |_ -> Seq.empty
                    ) 
            |> Seq.filter (fun x-> not (Seq.isEmpty x))

    Azure.Containers.CloudBlobClient.ListContainers () 
        |> Seq.filter (fun x-> isTargetContainerPredicate x) 
        |> Seq.map (fun y->y.ListBlobs ("", true)) 
        |> Seq.collect (fun x->x) (*flattern approach1*) 
        |> Seq.filter (fun x-> (x :? Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob) && isTargetBlobGzLogPredicate ( x :?> Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob )) 
        |> Seq.map (fun x-> x :?> Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob) 
        |> Seq.iter (fun x->printf "Deleting:%A:%A..."x.Container.Name x.Name; x.Delete (); printfn "Deleted:%A:%A..."x.Container.Name x.Name; )
        
    ignore
