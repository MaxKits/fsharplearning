﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.

[<EntryPoint>]
let main argv = 
    let arglist = argv |> List.ofSeq
    let fromTime = System.DateTime.ParseExact (arglist.Item 0,"yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture ,System.Globalization.DateTimeStyles.None)
    let toTime = System.DateTime.ParseExact (arglist.Item 1 ,"yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture ,System.Globalization.DateTimeStyles.None)

    let r = AzureStorageCleaner.findFiles fromTime toTime <| arglist.Item 2

    printfn "%A" argv
    0 // return an integer exit code
