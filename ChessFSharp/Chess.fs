﻿module Chess
open Microsoft.FSharp.Reflection
open ChessTypes
open ChessConsole

let GenerateFigures =
    seq {for i in [Rank.Elephant;Rank.Hourse;Rank.King;Rank.Pawn;Rank.Queen;Rank.Rook] do yield![Figure(i,{x='a';y=1},true); Figure(i,{x='a';y=1},false)] }
    |> Seq.collect (fun x->seq {for i in x.GetInitPosition -> Figure(x.Rank,i,x.IsWhite)  }  )

///////////////////////////////////////////////////logic
    
let thereIsNoFigureOnCell (figures:list<Figure>) (pos:Position) = 
    figures |> List.tryFind (fun x->x.Position=pos) |> (fun x-> if' x.IsSome false true)

let thereIsFigureOnCell (figures:list<Figure>) (pos:Position) = 
    figures |> List.tryFind (fun x->x.Position=pos) |> (fun x-> if' x.IsSome true false)

let whatIsTheFigureOnCell (figures:list<Figure>) (pos:Position) = 
    figures |> List.tryFind (fun x->x.Position=pos)

let isItPossible2 (pos1:Position) (pos2:Position) (figures:list<Figure>) (figure:Figure) isWhiteNow = 
    let toEnemyDirection = if' isWhiteNow (pos2.y-pos1.y < 0) (pos2.y-pos1.y > 0)
    let figureDoesntMovedHorizontical = ((pos1.x = pos2.x) && thereIsNoFigureOnCell figures pos2)

    let figureMovedHorizontical = abs( (int pos1.x) - (int pos2.x) ) >0
    let figureMovedVertical = abs( pos2.y-pos1.y ) >0

    let getHorizontalLength = abs( (int pos1.x) - (int pos2.x) ) 
    let getVerticalLengthIs = abs( pos2.y-pos1.y ) 

    let horizontalLengthIs x = getHorizontalLength = x
    let verticalLengthIs x = getVerticalLengthIs = x

    let thereIsNoFigureOnTargetCell = thereIsNoFigureOnCell figures pos2
    let thereIsEnemyFigureOnTargetCell = thereIsFigureOnCell figures pos2 && (whatIsTheFigureOnCell figures pos2).Value.IsWhite <> isWhiteNow 
    
    match figure.Rank with
        | Rank.Pawn -> toEnemyDirection && verticalLengthIs 1  && ( (figureDoesntMovedHorizontical && thereIsNoFigureOnTargetCell) || (figureMovedHorizontical && thereIsEnemyFigureOnTargetCell) )        
        | Rank.Rook -> figureMovedHorizontical <> figureMovedHorizontical          
        | Rank.Elephant -> getHorizontalLength = getVerticalLengthIs        
        | Rank.Queen -> (getHorizontalLength = getVerticalLengthIs) || (figureMovedHorizontical <> figureMovedHorizontical)        
        | Rank.King -> ( (getHorizontalLength = getVerticalLengthIs) && (verticalLengthIs 1)) || ( (figureMovedHorizontical <> figureMovedHorizontical) && (verticalLengthIs 1 || horizontalLengthIs 1) )
        |_ -> false

let IsItPossible (resParsed:Position*Position) (figures:list<Figure>) isWhiteNow= 
    let exists = figures |> List.tryFind (fun x->x.Position=fst resParsed) |> (fun x-> if x.IsSome then x.Value.IsWhite=isWhiteNow else false)
    let existsAndInRange =exists && (snd resParsed).x >= 'a' && (snd resParsed).x <= 'h' && (snd resParsed).y >= 1 &&  (snd resParsed).y <= 8
    //add enemu here
    let possibleToStay = existsAndInRange && (isItPossible2 (fst resParsed) (snd resParsed) figures (figures |> List.tryFind (fun x->x.Position=fst resParsed)).Value isWhiteNow)

    let res =  possibleToStay
    res

let rec Step (figures:list<Figure>) isWhiteNow = 
    System.Console.BackgroundColor <- System.ConsoleColor.Black
    System.Console.WriteLine("Print in format a1-a2")
    let resParsed = System.Console.ReadLine() |> (fun s -> (toPosition (s.Chars 0) (s.Chars 1) , toPosition (s.Chars 3) (s.Chars 4)) )
    if IsItPossible resParsed figures isWhiteNow then
        let figures2 = figures |> List.map (fun x -> if (x.Position = (fst resParsed)) then Figure(x.Rank,snd resParsed,x.IsWhite) else x )
        PrintFigures figures2
        Step figures2 (not isWhiteNow)
    else
        let figures2 = figures
        PrintFigures figures2
        System.Console.WriteLine("Impossible Step. Try once more.")
        Step figures2 ( isWhiteNow)

    