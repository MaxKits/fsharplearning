﻿module ChessTypes
open Microsoft.FSharp.Reflection

type Position = {y:int;x:char;}

let third (_,_,x,_) = x
let fourth (_,_,_,x) = x
let toPosition (c1:char) (c2:char) = {x=c1;y=System.Int32.Parse (c2.ToString())}

let if' c th1 th2 = if c then th1 else th2

type Rank = 
        | Pawn
        | Hourse
        | Elephant
        | Rook
        | Queen
        | King

type Figure (r:Rank,p:Position,iw:bool) = 
    member this.Rank = r
    member this.Position = p
    member this.IsWhite = iw
    member this.GetInitPosition = 
        match this.Rank with
        | Pawn -> seq { for i in 'a'..'h' ->if this.IsWhite then {x=i;y=7} else {x=i;y=2}}       
        | Hourse -> seq { for i in ['b';'g'] -> if this.IsWhite then {x=i;y=8} else {x=i;y=1 }}
        | Elephant -> seq { for i in ['c';'f'] -> if this.IsWhite then {x=i;y=8} else {x=i;y=1 }}
        | Rook -> seq { for i in ['a';'h'] -> if this.IsWhite then {x=i;y=8} else {x=i;y=1 }}
        | Queen when this.IsWhite -> seq[ {x='d';y=8}]
        | Queen when this.IsWhite=false -> seq[{x='d';y=1}]
        | King  -> if this.IsWhite then seq[{x='e';y=8}] else seq[{x='e';y=1}]
    member this.SetPosition p = 
        this.Position = p
        
