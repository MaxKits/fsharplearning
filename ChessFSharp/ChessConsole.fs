﻿module ChessConsole
open Microsoft.FSharp.Reflection
open ChessTypes

let SetBackColorForFigure y count = 
    match (y+count)%2 with
        | 1 -> System.Console.BackgroundColor <- System.ConsoleColor.DarkGray
        | 0 -> System.Console.BackgroundColor <- System.ConsoleColor.Gray

let SetColorForFigure (x:Figure) = 
    match x.IsWhite with
    | true -> System.Console.ForegroundColor <- System.ConsoleColor.Blue 
    | false-> System.Console.ForegroundColor <- System.ConsoleColor.Red

let GetSymbolForFigure (x:Figure) = 
    match x.Rank with
        | Pawn -> 'i'       
        | Hourse -> '?'
        | Elephant -> '^'
        | Rook ->  'U'
        | Queen -> 'Q'
        | King -> 'K'

let PrintC (c:char*int) i =
    let backColor = SetBackColorForFigure i (snd c)
    match c with
        | ('h',z)->System.Console.WriteLine( " " )
        | (_,z)  -> System.Console.Write( " " )

let PrintF (x:Figure) i = 
    let color = SetColorForFigure x
    let backColor = SetBackColorForFigure x.Position.y i
    let symbol = GetSymbolForFigure x
    match x.Position with
        | {x='h';y=z}->System.Console.WriteLine( symbol )
        | {x=_;y=z}->System.Console.Write( symbol )

let rec PickFigureOrCell (figures:list<Figure>) (cells:list<char*int>) =
    match (figures,cells) with
        | ([],[])->[]
        | (f,[])-> List.map (fun x-> (Some(x),None)) f
        | ([],c)-> List.map (fun x-> (None,Some(x))) c
        | (hf::tf,hc::tc) when hf.Position.x = fst hc && hf.Position.y = snd hc -> (Some(hf),None):: PickFigureOrCell tf tc
        | (hf::tf,hc::tc) when hf.Position.x <> fst hc || hf.Position.y <> snd hc -> (None,Some(hc)):: PickFigureOrCell (hf::tf) tc

let PrintFigures (figures:seq<Figure>) =
    let cells = seq {for i in 1..8 do yield! seq {for j in 'a'..'h' -> (j,i)}} |> Seq.toList |> List.sortBy (fun x -> -(snd x), fst x)
    let sortedFigures = figures  |> Seq.toList |> List.sortBy (fun x -> -( x.Position.y),  x.Position.x)
    let figureEitherCell = PickFigureOrCell sortedFigures cells
    let res = figureEitherCell
                |> Seq.iteri (fun i x -> if (fst x).IsSome then PrintF (fst x).Value i else PrintC (snd x).Value i)
    1
