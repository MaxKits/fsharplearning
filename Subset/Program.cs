﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Subset
{
	internal class Program
	{
		private static void Main( string[] args )
		{
			Console.WriteLine( "findSubsetsIterative" );
			ExecuteAndWriteMetrics(() => findSubsetsIterative(Tuple.Create(2, GenerateSOurceData(1000).Length), GenerateSOurceData(1000000), (sourceSet, indiciesToSum) => indiciesToSum.Select(y => sourceSet[y]).Sum() == 293));
			//Console.WriteLine( "findSubsetsRecoursive" );
			//ExecuteAndWriteMetrics(() => findSubsetsRecoursive(Tuple.Create(2, 293), GenerateSOurceData(1000).ToList(), new List<int>(), new List<int>()));
			Console.ReadLine();
		}

		public static void ExecuteAndWriteMetrics( Func< IEnumerable< IEnumerable< int > > > act )
		{
			var stopwatch = Stopwatch.StartNew();
			var res = act();
			stopwatch.Stop();
			Console.WriteLine( "ElapsedMilliseconds: " + stopwatch.ElapsedMilliseconds );
			//PrintRes( res );
		}

		public static void PrintRes( IEnumerable< IEnumerable< int > > r )
		{
			foreach( var item in r )
			{
				foreach( var x in item )
				{
					Console.Write( x.ToString() + "," );
				}
				Console.WriteLine();
			}
		}

		public static IEnumerable< int[] > findSubsetsIterative( Tuple< int, int > ks, int[] sourceSet, Func< int[], int[], bool > specialCondition )
		{
			var a = new int[ ks.Item1 ];
			for( int i = 0; i < ks.Item1; i++ )
			{
				a[ i ] = i; //we store indicies instead of values
			}

			var p = ks.Item1 - 1;

			while( p >= 0 )
			{
				//>a
				// last item from source included to current subset
				if( specialCondition( sourceSet, a ) )
					yield return a.ToArray();
				//Console.WriteLine(a);
				if( a[ ks.Item1 - 1 ] == sourceSet.Length - 1 )
					p--;
				else
					p = ks.Item1 - 1;

				if( p >= 0 )
				{
					for( int i = ks.Item1 - 1; i >= p; i-- )
					{
						a[ i ] = a[ p ] + i - p + 1;
					}
				}
			}
		}

		public static IEnumerable< List< int > > findSubsetsRecoursive( Tuple< int, int > ks, List< int > set, List< int > subSet, List< int > subSetIndex )
		{
			if( ks.Item1 == 0 && ks.Item2 == 0 )
			{
				var res = new List< List< int > >();
				res.Add( subSetIndex );
				return res;
			}
			else if( ks.Item1 > 0 && ks.Item2 > 0 )
			{
				var res = set.Select( ( x, i ) =>
				{
					var newSubset = subSet.Select( y => y ).ToList();
					newSubset.Add( x );

					var newSubsetIndex = subSetIndex.Select( y => y ).ToList();
					newSubsetIndex.Add( i );

					var newSet = set.Skip( i ).ToList();
					return findSubsetsRecoursive( Tuple.Create( ks.Item1 - 1, ks.Item2 - x ), newSet, newSubset, newSubsetIndex ).ToList();
				}
					).SelectMany( x => x ).ToList();
				return res;
			}
			else
				return new List< List< int > >();
		}
		public static int[] GenerateSOurceData(int length)
		{
			var a = new[]
			{
				//1, 2, 3
				7, 286, 200,
				176, 120, 165, 206, 75, 129, 109,
				123, 111, 43, 52, 99, 128, 111, 110, 98, 135,
				112, 78, 118, 64, 77, 227, 93, 88, 69, 60,
				34, 30, 73, 54, 45, 83, 182, 88, 75, 85,
				54, 53, 89, 59, 37, 35, 38, 29, 18, 45,
				60, 49, 62, 55, 78, 96, 29, 22, 24, 13,
				14, 11, 11, 18, 12, 12, 30, 52, 52, 44,
				28, 28, 20, 56, 40, 31, 50, 40, 46, 42,
				29, 19, 36, 25, 22, 17, 19, 26, 30, 20,
				15, 21, 11, 8, 8, 19, 5, 8, 8, 11,
				11, 8, 3, 9, 5, 4, 7, 3, 6, 3,
				5, 4, 5, 6
			};
			var z = a.Length * length;
			var res = new int[z];
			for (int i = 0; i < length; i++)
			{
				a.CopyTo(res, i * a.Length);
			}

			return res;
		}
	}
}