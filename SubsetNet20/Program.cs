﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace SubsetNet20
{
	internal class Program
	{
		private static void Main( string[] args )
		{
			Console.WriteLine( "findSubsetsIterative( 5, GenerateSOurceData(1), Summ, 60 )" );
			int[][] res1;

			var generateSOurceData = GenerateSOurceData( 1 );
			 var expected = 313;
			 var K = 7;


			//var expected = 21;
			//var K = 3;

			using( var fileStream = new FileStream( "f:\\y.txt", FileMode.Append ) )
			using( var streamWriter = new StreamWriter( fileStream ) )
			{
				var puzzle = new Puzzle( GenerateSOurceData( 1 ) );

				Console.WriteLine( "puzzle.Solve(5, 60, PuzzleOnSubsetFound)" );
				ExecuteAndWriteMetrics( () =>
				{
					puzzle.Solve(K, expected, Puzzle.PuzzleOnSubsetFoundToFile, streamWriter);
					return null;
				} );
			}

			ExecuteAndWriteMetrics( () =>
			{
				var res = MaximsMethod.findSubsetsIterative( K, generateSOurceData, MaximsMethod.SummshouldBe, expected );
				res1 = res.SubSets;
				//PrintResult( res1 );
				return res1;
			} );

			Console.ReadLine();
		}

		public static void ExecuteAndWriteMetrics( MaximsMethod.findSubset act )
		{
			var stopwatch = Stopwatch.StartNew();
			var res = act();
			stopwatch.Stop();
			Console.WriteLine( "ElapsedMilliseconds: " + stopwatch.ElapsedMilliseconds );
			Console.WriteLine( "Elapsed: " + stopwatch.Elapsed );
			//PrintRes( res );
		}

		//public static void PrintResult( IEnumerable< IEnumerable< int > > r, string fname )
		public static void PrintResult( int[][] r )
		{
			using( var fileStream = new FileStream( MaximsMethod.MAXFNAME, FileMode.Append ) )
			using( var streamWriter = new StreamWriter( fileStream ) )
			{
				foreach( var item in r )
				{
					if( item == null )
						break;

					MaximsMethod.SortAndPrintToFileToAFile( item, streamWriter );
				}

				streamWriter.Flush();
				fileStream.Flush();
			}
		}

		public static void PrintToConsole( IEnumerable< IEnumerable< int > > r )
		{
			foreach( var item in r )
			{
				foreach( var x in item )
				{
					Console.Write( x.ToString() + "," );
				}
				Console.WriteLine();
			}
		}

		public static int[] GenerateSOurceData( int length )
		{
			var a = new[]
			{
				//1, 2, 3
				7, 286, 200,
				176, 120, 165, 206, 75, 129, 109,
				123, 111, 43, 52, 99, 128, 111, 110, 98, 135,
				112, 78, 118, 64, 77, 227, 93, 88, 69, 60,
				34, 30, 73, 54, 45, 83, 182, 88, 75, 85,
				54, 53, 89, 59, 37, 35, 38, 29, 18, 45,
				60, 49, 62, 55, 78, 96, 29, 22, 24, 13,
				14, 11, 11, 18, 12, 12, 30, 52, 52, 44,
				28, 28, 20, 56, 40, 31, 50, 40, 46, 42,
				29, 19, 36, 25, 22, 17, 19, 26, 30, 20,
				15, 21, 11, 8, 8, 19, 5, 8, 8, 11,
				11, 8, 3, 9, 5, 4, 7, 3, 6, 3,
				5, 4, 5, 6
			};
			var z = a.Length * length;
			var res = new int[ z ];
			for( int i = 0; i < length; i++ )
			{
				a.CopyTo( res, i * a.Length );
			}

			return res;
		}
	}
}