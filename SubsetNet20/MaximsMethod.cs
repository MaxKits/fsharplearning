using System;
using System.IO;
using System.Text;

namespace SubsetNet20
{
	public class Result
	{
		public int ResCount { get; set; }
		public int[][] SubSets { get; set; }

		public Result()
		{
			this.ResCount = 0;
			this.SubSets = new int[ Int32.MaxValue / 8 - 8 ][];
		}
	}

	internal class MaximsMethod
	{
		public static Result findSubsetsIterative( int k, int[] sourceSet, findSubsetsIterativeFilter specialCondition, int expected )
		{
			var resultsCount = 0;

			//decimal aproxCount =1;
			//for (int i = k + 1; i <= sourceSet.Length; i++)
			//{
			//	aproxCount = (aproxCount*(ulong)i)/(i-k);
			//}

			using( var v = new FileStream( MAXFNAME, FileMode.Append ) )
			using( var write = new StreamWriter( v ) )
			{
				var res = new Result();

				var a = new int[ k ];
				for( int i = 0; i < k; i++ )
				{
					a[ i ] = i;
				}

				var kDecremented = k - 1;
				var p = kDecremented;
				while( p >= 0 )
				{
					if( specialCondition( sourceSet, a, expected ) )
					{
						var temp = ( int[] )a.Clone();
						//res.SubSets[ resultsCount ] = temp;
						//resultsCount++;
						SortAndPrintToFileToAFile( temp, write );
					}

					p = ( a[ kDecremented ] == sourceSet.Length - 1 ) ? p - 1 : kDecremented;
					if( p >= 0 )
					{
						for( int j = kDecremented; j >= p; j-- )
						{
							a[ j ] = a[ p ] + j - p + 1;
						}
					}
				}
				res.ResCount = resultsCount;
				return res;
			}
		}

		public static bool SummshouldBe( int[] sourceSet, int[] indicies, int expected )
		{
			var sum = 0;
			for( int i = 0; i < indicies.Length; i++ )
			{
				sum += sourceSet[ indicies[ i ] ];
			}
			return sum == expected;
		}

		public delegate int[][] findSubset();

		public delegate bool findSubsetsIterativeFilter( int[] sourceSet, int[] indiciesToSum, int expected );

		public static void SortAndPrintToFileToAFile( int[] item, StreamWriter write )
		{
			var sb = new StringBuilder();
			Array.Sort( item );
			foreach( var x in item )
			{
				sb.Append( x + "," );
			}
			write.WriteLine( sb );
			write.Flush();
		}

		public const string MAXFNAME = "F:\\Max.txt";
	}
}