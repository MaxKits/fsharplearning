﻿module Fold

// Sum
let sumSeq sequence1 = Seq.fold (fun acc elem -> acc + elem) 0 sequence1

let FoldSumSeqExample n =
    Seq.init n (fun index -> index * index)
        |> sumSeq
        |> printfn "The sum of the elements is %d."
    ()

// Fibonachi
let FibonachiStep (a,b) = 
    printfn "Calles with %d-%d." a b
    (b,a+b)
     
let finbonach sequence1 = Seq.fold (fun acc (a,b)-> FibonachiStep(acc)) (0,1) sequence1
    
let FoldFibonachExample n =
    Seq.init n (fun index -> (index,index) )
        |> finbonach
        |> fst
        |> printfn "Last item in Fibonach is %d."
    ()
// unfold
let UnFoldFibonachExampleStep (a, b) =
    if a + b > 100 then
        None
    else
        let nextValue = a + b
        Some(nextValue,(nextValue, a))

let finbonachUnfold  = Seq.unfold (fun (a,b)-> UnFoldFibonachExampleStep (a,b)) (0,1) 
    
let UnFoldFibonachExample =
    finbonachUnfold
        |> Seq.iter ( fun x-> printfn "Item in Fibonach is %d." x)
    ()


