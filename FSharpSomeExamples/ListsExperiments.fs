﻿module ListsExperiments

// Sum
let IterateListWhileItIsPossible = 
    let step = 1024 * 1024
    let shortList = [ 1..step ] |> List.map (fun x -> (0, x))
    let longList = ref [ (0, 0) ]
    let counter1 = ref 0
    let counter2 = ref 0
    while true do
        if (System.Int32.MaxValue - !counter1) < step then 
            incr counter2
            counter1 := 0
        counter1 := !counter1 + step
        longList := List.append (!longList) shortList
        System.Console.WriteLine("{0}-{1}", !counter1, !counter2)
    0

let arrayIndexer = 
    let a = [| 1..10 |]
    let b = [| 1..10 |]
    let mutable c = 0
    while c < 10 do
        System.Console.WriteLine("a[" + c.ToString() + "]=" + a.[c].ToString())
        System.Console.WriteLine("a[" + c.ToString() + "]=" + b.[c].ToString())
        c <- c + 1
