﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.

let rec convStr (prevSymb:char) (xh::xt) = 
    match prevSymb, xh::xt with
        | _,_::[] ->[xh]
        | _,' '::_   -> convStr xh xt  
        | _,':'::_ -> '_' :: (convStr  xh xt)
        | ' ',a::_-> a.ToString().ToUpper().Chars 0 :: (convStr xh xt )
        | b,a::_ -> a :: (convStr  xh xt)


[<EntryPoint>]
let main argv = 
    match argv with
        | [|"about"|]  ->  System.Console.WriteLine "(с) Max Kitsenko, 2015"
        | [||]  ->  System.Console.WriteLine "USAGE: string {string}"
                    System.Console.WriteLine "EXAMPLE: >StringFromJiraToGit \"max kits\" \"m :kitsenko\""
        | x     ->  let res = argv |> Array.map( fun x -> convStr ' ' (x |> Seq.toList ) )  //because strings are char seqs
                    Array.iter (fun x -> Array.ofList(x) |> System.Console.WriteLine ) res
    0 // return an integer exit code
