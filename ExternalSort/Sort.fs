﻿module Sort

open System.IO

let maxTempFileSize = 100 * 1024 * 1024



let SortRowInFile(fname : string) = 
    seq { 
        use sr = new StreamReader(fname)
        while not sr.EndOfStream do
            yield sr.ReadLine()
    }
    |> Seq.sort
    |> Seq.iter (new StreamWriter(fname + "temp")).WriteLine
    File.Delete fname
    File.Replace(fname + "temp", fname, fname + "tempBack")
    fname

let ReadFirst100Rows(sr : StreamReader) = 
    let c = ref 0
    seq { 
        while not sr.EndOfStream && !c < 100 do
            yield sr.ReadLine()
    }

//use monad!
let rec MergeToFiles (sw : StreamWriter) (listOfLists : List<List<string>>) = 
    if (listOfLists
        |> List.map (fun x -> List.length x)
        |> List.fold (+) 0)
       > 0
    then 
        let smallestElement = 
            listOfLists
            |> List.mapi (fun i x -> (List.head x, i))
            |> List.sortBy (fun x -> fst x)
            |> List.head
        sw.WriteLine(fst smallestElement)
        MergeToFiles sw (listOfLists |> List.mapi (fun i x -> 
                                            if i = (snd smallestElement) then List.tail x
                                            else x))
    else ()

let MergeIntoOneFile  (newfName : string) (fnames : List<string>) = 
    let sreaders = fnames |> List.map (fun x -> new StreamReader(x))
    use nf = new StreamWriter(newfName)
    while not (sreaders
               |> List.map (fun x -> x.EndOfStream)
               |> List.fold (&&) true) do
        sreaders
        |> List.map ReadFirst100Rows
        |> List.map Seq.toList
        |> MergeToFiles nf
    ()

let DevideFile(fname : string) :List<string> = 
    let i = ref 0
    use sr = new StreamReader(fname)    
    let mutable fileName = String.concat "" [ fname;  (!i).ToString() ]
    use mutable sw = 
        new StreamWriter(fileName)
    let result = ref List.Empty

    while not sr.EndOfStream = false do
        sr.ReadLine() |> sw.WriteLine
        if sw.BaseStream.Length >= (int64) maxTempFileSize then 
            result := fileName::!result
            sw.Close()
            incr i
            fileName <- String.concat "" [ fname; (!i).ToString() ]
            sw <- new StreamWriter (fileName)
            ()
    !result


//todo monad
let SortLinesInFile fname = 
    fname|> DevideFile |> Seq.map (fun x -> SortRowInFile x) |> Seq.toList |> MergeIntoOneFile "result.txt"
    ()

