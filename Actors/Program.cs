﻿using System;
using System.Linq;
using Akka.Configuration;

namespace Actors
{
	internal class Program
	{
		private static void Main( string[] args )
		{
			if( args != null && args.Any() && args[ 0 ].ToLower() == "server" )
			{
				Console.WriteLine( "Server started" );

				Func< Config > ActorConfigRemoteFactory = () =>
				{
					var config = ConfigurationFactory.ParseString( @"
				akka {
				    actor {
				        provider = ""Akka.Remote.RemoteActorRefProvider, Akka.Remote""
				    }
				
				    remote {
				        helios.tcp {
				            port = 8080
				            hostname = 127.0.0.1
				        }
				    }
				}
				" );
					return config;
				};

				Func< Config > ActorConfigLocalFactory;
				if( args.Count() > 1 && args[ 1 ] != null )
				{
					ActorConfigLocalFactory = () =>
					{
						var hocon = @"
				akka {
				    actor {
				        provider = ""Akka.Remote.RemoteActorRefProvider, Akka.Remote""
				    }
				
				    remote {
				        helios.tcp {
				            port = 8080
				            hostname = localhost
				        }
				    }
				}
				";
						hocon = hocon.Replace("localhost", "\"" + args[1] + "\"");
						var config = ConfigurationFactory.ParseString( hocon );
						return config;
					};
				}
				else
				{
					ActorConfigLocalFactory = () =>
					{
						var config = ConfigurationFactory.ParseString( @"
akka {
    actor {
        provider = ""Akka.Remote.RemoteActorRefProvider, Akka.Remote""
    }

    remote {
        helios.tcp {
            port = 8080
            hostname = localhost
        }
    }
}
				" );
						return config;
					};
				}

				var processor = new Server( ActorConfigLocalFactory );
				processor.Start();
			}
			else if( args != null && args.Any() && args[ 0 ].ToLower() == "client" )
			{
				Console.WriteLine( "Client started" );

				Func< Config > ActorConfigLocalFactory = () =>
				{
					var config = ConfigurationFactory.ParseString( @"
						akka {
						    actor {
						        provider = ""Akka.Remote.RemoteActorRefProvider, Akka.Remote""
						    }
						    remote {
						        helios.tcp {
						            port = 8090
						            hostname = localhost
						        }
						    }
						}
				" );
					return config;
				};

				var processor = new Server( ActorConfigLocalFactory );
				if( args.Count() > 1 && args[ 1 ] != null )
					processor.StartClient( args[ 1 ] );
				else
					processor.StartClient();
			}
		}
	}
}