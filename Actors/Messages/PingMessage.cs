﻿namespace Actors.Messages
{
	public class PingMessage
	{
		public PingMessage( string who )
		{
			this.Who = who;
		}

		public string Who { get; private set; }
	}
}