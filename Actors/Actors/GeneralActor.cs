﻿using System;
using Actors.Messages;
using Akka.Actor;

namespace Actors.Actors
{
	public class GeneralActor : ReceiveActor
	{
		public GeneralActor()
		{
			// Tell the actor to respond
			// to the Greet message
			this.Receive< PingMessage >( greet =>
				Console.WriteLine( "Actor: Hello {0}", greet.Who ) );
		}
	}
}