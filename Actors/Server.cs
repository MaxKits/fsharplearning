﻿using System;
using Actors.Actors;
using Actors.Messages;
using Akka.Actor;
using Akka.Configuration;

namespace Actors
{
	public class Server
	{
		public Server( Func< Config > actorConfigFactory )
		{
			this.ActorConfigFactory = actorConfigFactory;
		}

		public Func< Config > ActorConfigFactory { get; private set; }

		public void Start()
		{
			var system = this.ActorConfigFactory != null ? ActorSystem.Create( "MyServer", this.ActorConfigFactory() ) : ActorSystem.Create( "MyServer" );
			using( system )
			{
				system.ActorOf< GeneralActor >( "greeter" );
				Console.ReadKey();
			}
		}

		public void StartClient( string listenServer = "akka.tcp://MyServer@localhost:8080/user/greeter" )
		{
			using( var system = this.ActorConfigFactory != null ? ActorSystem.Create( "MyClient", this.ActorConfigFactory() ) : ActorSystem.Create( "MyClient" ) )
			{
				//get a reference to the remote actor
				var greeter = system.ActorSelection( listenServer );
				//send a message to the remote actor
				greeter.Tell( new PingMessage( "World" ) );
				Console.ReadLine();
			}
		}
	}
}